package staticProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see dynamicProxy <br>
 */
public class Fish implements Animal{
    Sheep sheep = new Sheep();
    @Override
    public void show() {
        sheep.show();
        System.out.println("Fish love earthworm");
    }
}