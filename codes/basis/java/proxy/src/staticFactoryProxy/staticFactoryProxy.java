package staticFactoryProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticFactoryProxy <br>
 */
public class staticFactoryProxy {
    public static void main(String[] args) {
        TaoBao taoBao=new TaoBao();
        float price=taoBao.sell(100);
        System.out.println("淘宝的销售价："+price);
    }
}