package staticFactoryProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticFactoryProxy <br>
 */
public class TaoBao implements UsbSell{
    private UsbSell factory = new UsbFactory();
    @Override
    public float sell(float price) {
        float p = factory.sell(price);
        float p1 = p + 25;
        System.out.println("return $5 coupon");
        return p1;
    }
}