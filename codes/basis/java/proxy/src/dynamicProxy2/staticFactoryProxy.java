package dynamicProxy2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticFactoryProxy <br>
 */
public class staticFactoryProxy {
    public static void main(String[] args) {
        //创建目标对象
        UsbSell usbFactory = new UsbFactory();
        //创建invocationHandler对象
        InvocationHandler invocationHandler = new MySellHandler(usbFactory);
        //创建代理对象
        UsbSell proxy= (UsbSell) Proxy.newProxyInstance(
                usbFactory.getClass().getClassLoader(),
                usbFactory.getClass().getInterfaces(),
                invocationHandler
        );
        System.out.println(proxy.sell(100));
    }
}