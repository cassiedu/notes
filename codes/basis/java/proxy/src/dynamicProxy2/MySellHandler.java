package dynamicProxy2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see dynamicProxy2 <br>
 */
//必须实现InvocationHandler接口，完成代理类的功能（调用目标方法、功能增强）
public class MySellHandler implements InvocationHandler {
    private  Object target = null;
    //动态代理的目标对象是活动的，需要传入进来，传进来的是谁就给谁创建代理
    public MySellHandler(Object target){
        this.target = target;
    }
    //args代表传进来的参数（100）
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object res = null;
        res = method.invoke(target,args);
        if(res != null){
            Float price = (Float)res;
            price = price + 25;
            res = price;
        }
        System.out.println("淘宝商家返回5元优惠券");
        return res;
    }
}