package dynamicProxy2;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticFactoryProxy <br>
 */
public interface UsbSell {
    float sell(float price);

    //添加接口方法
    void hello();
}