package dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see dynamicProxy <br>
 */
public class Fish implements Animal {
    @Override
    public void show() {
        Animal objectProxy = (Animal) Proxy.newProxyInstance(//创建接口实例
                Animal.class.getClassLoader(),//与目标对象有相同的类加载器，动态代理类运行时创建，将类加载到内存(反射)
                new Class[]{Animal.class},//被代理的类所实现的接口（可以是多个）
                new InvocationHandler() {//绑定代理类的方法（我们自己写的，代理类要完成的功能）
                    @Override//提供invoke方法，代理类的每一个方法执行时，都将调用一次invoke
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("i am ahead");//proxy：代理后的实例对象 method：对象被调用的方法 args：调用时的参数
                        method.invoke(new Sheep(), args);//执行目标对象的方法
                        System.out.println("i am behind");
                        return null;
                    }
                }
        );
        objectProxy.show();
    }
}