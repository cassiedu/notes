package dynamicProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see dynamicProxy <br>
 */
public class DynamicProxyTest {
    public static void main(String[] args) {
        Fish fish = new Fish();
        fish.show();
    }
}