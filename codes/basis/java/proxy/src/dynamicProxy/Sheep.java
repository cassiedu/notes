package dynamicProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see dynamicProxy <br>
 */
public class Sheep implements Animal {
    @Override
    public void show() {
        System.out.println("sheep love grass");
    }
}