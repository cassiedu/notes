package staticReflextionProxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticReflextionProxy <br>
 */
public class ReflectionTest {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //使用反射机制执行sayHello方法，核心是Method类中的方法
        HelloService target = new HelloServiceImpl();
        //获取sayHello名称对于Method的对象
        Method method = HelloService.class.getMethod("sayHello", String.class);
        //通过Method可以执行方法的调用
        //invoke是Method类中的一个方法，表示添加方法的调用，
        //参数1：表示对象，要执行这个对象的方法
        //参数2：方法执行的时候的参数值
        //参数3：方法要执行的时候的返回值
        //表示执行target对象的sayHello方法，参数是zhai，method代表的是执行的方法
        Object ret = method.invoke(target,"zhai");
        
        HelloService target2= new HelloServiceImpl2();
        Object ret2 = method.invoke(target2,"zhai");
    }
}