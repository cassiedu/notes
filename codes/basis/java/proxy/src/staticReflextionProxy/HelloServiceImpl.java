package staticReflextionProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticReflextionProxy <br>
 */
public class HelloServiceImpl  implements HelloService {
    public void sayHello(String name){
        System.out.println("hello"+name);
    }
}