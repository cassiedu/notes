package staticReflextionProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticReflextionProxy <br>
 */
public class Test {
    public static void main(String[] args) {
        HelloService helloService=new HelloServiceImpl();
        helloService.sayHello("zhai");
    }
}