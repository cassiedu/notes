package staticReflextionProxy;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2020年11月10日<br>
 * @see staticReflextionProxy <br>
 */
public interface HelloService {
    void sayHello(String name);
}
