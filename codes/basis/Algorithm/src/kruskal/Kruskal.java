package kruskal;

import java.util.Arrays;

public class Kruskal {
    static char[] vertex = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
    static final int INF = Integer.MAX_VALUE;

    public static void main(String[] args) {
        int matrix[][] = {
                /*A*//*B*//*C*//*D*//*E*//*F*//*G*/
                /*A*/ {0, 12, INF, INF, INF, 16, 14},
                /*B*/ {12, 0, 10, INF, INF, 7, INF},
                /*C*/ {INF, 10, 0, 3, 5, 6, INF},
                /*D*/ {INF, INF, 3, 0, 4, INF, INF},
                /*E*/ {INF, INF, 5, 4, 0, 2, 8},
                /*F*/ {16, 7, 6, INF, 2, 0, 9},
                /*G*/ {14, INF, INF, INF, 8, 9, 0}};
        Graph graph = new Graph(vertex, matrix);
        graph.print();
        graph.krusal();

    }
}

class Graph {
    char[] vertex;
    int[][] weight;
    int edgeNum;

    public Graph(char[] vertex, int[][] weight) {
        int verNum = vertex.length;

        this.vertex = new char[verNum];
        for (int i = 0; i < vertex.length; i++) {
            this.vertex[i] = vertex[i];
        }

        this.weight = new int[verNum][verNum];
        for (int i = 0; i < verNum; i++) {
            for (int j = 0; j < verNum; j++) {
                this.weight[i][j] = weight[i][j];
            }
        }

        for (int i = 0; i < verNum; i++) {
            for (int j = i + 1; j < verNum; j++) {
                if (this.weight[i][j] != Kruskal.INF) {
                    edgeNum++;
                }
            }
        }
    }

    public void print() {
        System.out.println("邻接矩阵为: \n");
        for (int i = 0; i < vertex.length; i++) {
            for (int j = 0; j < vertex.length; j++) {
                System.out.printf("%12d", weight[i][j]);
            }
            System.out.println();//换行
        }
    }

    //边的信息转换为结点
    public Edge[] getEdge() {
        int num = 0;
        Edge[] edge = new Edge[edgeNum];
        for (int i = 0; i < vertex.length; i++) {
            for (int j = i + 1; j < vertex.length; j++) {
                if (weight[i][j] != Kruskal.INF) {
                    edge[num++] = new Edge(vertex[i], vertex[j], weight[i][j]);
                }
            }
        }
        return edge;
    }

    public Edge[] sortWeight(Edge[] edge) {
        Edge temp;
        for (int i = 0; i < edge.length - 1; i++) {
            for (int j = 0; j < edge.length - 1 - i; j++) {
                if (edge[j].weight > edge[j + 1].weight) {
                    temp = edge[j];
                    edge[j] = edge[j + 1];
                    edge[j + 1] = temp;
                }
            }
        }
        return edge;
    }

    public int getVerIndex(char ch) {
        for (int i = 0; i < vertex.length; i++) {
            if (vertex[i] == ch) {
                return i;
            }
        }
        return -1;
    }

    public int getEnd(int[] end, int i) {
        while (end[i] != 0) {
            i = end[i];
        }
        return i;
    }

    public void krusal() {
        int index = 0;
        int[] end = new int[vertex.length];
        Edge[] res = new Edge[edgeNum];
        Edge[] edge = getEdge();
        System.out.println("排序前边的集合：" + Arrays.toString(edge));
        System.out.println("边的数量： " + edge.length);
        edge = sortWeight(edge);
        System.out.println("排序后边的集合：" + Arrays.toString(edge));
        //遍历edges 数组，将边添加到最小生成树中时，判断是准备加入的边否形成了回路，如果没有，就加入 rets, 否则不能加入
        for (int i = 0; i < edgeNum; i++) {
            int p1 = getVerIndex(edge[i].start);
            int p2 = getVerIndex(edge[i].end);
            int m = getEnd(end, p1);
            int n = getEnd(end, p2);
            if (m != n) {
                end[m] = n;
                res[index++] = edge[i];
//                System.out.println(res[index++]);
            }
        }
        System.out.println("minSpanningTree: ");
        for (int i = 0; i < index; i++) {
            System.out.println(res[i]);
        }
    }
}

class Edge {
    char start;
    char end;
    int weight;

    public Edge(char start, char end, int weight) {
        this.start = start;
        this.end = end;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "<" + start + "," + end + "> = " + weight;
    }
}
