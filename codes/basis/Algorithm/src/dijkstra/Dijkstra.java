package dijkstra;

import java.util.ArrayList;
import java.util.Arrays;

public class Dijkstra {
    public static void main(String[] args) {
        char[] city = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        int[][] distance = new int[][]{
                {10000, 5, 7, 10000, 10000, 10000, 2},
                {5, 10000, 10000, 9, 10000, 10000, 3},
                {7, 10000, 10000, 10000, 8, 10000, 10000},
                {10000, 9, 10000, 10000, 10000, 4, 10000},
                {10000, 10000, 8, 10000, 10000, 5, 4},
                {10000, 10000, 10000, 4, 5, 10000, 6},
                {2, 3, 10000, 10000, 4, 6, 10000},
        };
        int n = city.length;
        GraphDij graphDij = new GraphDij(n);
        graphDij.addVer(city);
        graphDij.addEdge(distance);
        graphDij.show();
        graphDij.dsj(2);
        graphDij.showDsj();
    }
}

class GraphDij {
    ArrayList<Character> vertex;
    int[][] edge;
    int n;
    private VertexVisited vv;

    public GraphDij(int n) {
        this.vertex = new ArrayList<>();
        this.edge = new int[n][n];
        this.n = n;
    }

    public void addVer(char[] ver) {
        for (char v : ver) {
            vertex.add(v);
        }
    }

    public void addEdge(int[][] wei) {
        for (int i = 0; i < wei.length; i++) {
            for (int j = 0; j < wei[0].length; j++) {
                edge[i][j] = wei[i][j];
            }
        }
    }

    public void show() {
        for (int ele : vertex) {
            System.out.print(ele + "\t");
        }
        System.out.println();
        for (int[] link : edge) {
            System.out.println(Arrays.toString(link));
        }
    }

    public void dsj(int index) {
        vv = new VertexVisited(n, index);
        update(index);
        for (int j = 1; j < n; j++) {
            index = vv.updateArr();
            update(index);
        }
    }

    private void update(int index) {
        int disSum = 0;
        for (int j = 0; j < edge[index].length; j++) {
            disSum = vv.getWeight(index) + edge[index][j];
            if (!vv.isVisited(j) && disSum < vv.getWeight(j)) {
                vv.updatePre(j, index);
                vv.updateDis(j, disSum);
            }
        }
    }

    public void showDsj() {
        vv.show();
    }

}

class VertexVisited {
    public int[] visited;
    public int[] pre;
    public int[] dis;

    public VertexVisited(int n, int index) {
        this.visited = new int[n];
        this.pre = new int[n];
        this.dis = new int[n];
        Arrays.fill(dis, 65535);
        this.visited[index] = 1;
        this.dis[index] = 0;
    }

    public int getWeight(int index) {
        return dis[index];
    }

    public boolean isVisited(int index) {
        return visited[index] == 1;
    }

    public void updatePre(int p, int index) {
        pre[p] = index;
    }

    public void updateDis(int index, int disSum) {
        dis[index] = disSum;
    }

    public int updateArr() {
        int min = 65535;
        int index = 0;
        for (int i = 0; i < visited.length; i++) {
            if (visited[i] == 0 && dis[i] < min) {
                min = dis[i];
                index = i;
            }
        }
        visited[index] = 1;
        return index;
    }

    public void show() {
        System.out.println("----------------");
        for (int vis : visited) {
            System.out.print(vis + "\t");
        }
        System.out.println();
        for (int i : pre) {
            System.out.print(i + "\t");
        }
        System.out.println();
        //输出dis
        for (int i : dis) {
            System.out.print(i + "\t");
        }
        System.out.println();

        char[] vertex = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        int count = 0;
        for (int i : dis) {
            if (i != 65535) {
                System.out.print(vertex[count] + "(" + i + ") ");
            } else {
                System.out.println("N ");
            }
            count++;
        }
        System.out.println();
    }
}