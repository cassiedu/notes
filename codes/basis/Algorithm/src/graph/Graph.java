package graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Graph {
    public static void main(String[] args) {
        String[] v = {"A", "B", "C", "D", "E", "F", "G", "H"};
        int n = v.length;

        GraphNode graphNode = new GraphNode(n);

        graphNode.addVertex(v);
        graphNode.addEdge(0, 1, 1);
        graphNode.addEdge(0, 2, 1);
        graphNode.addEdge(1, 3, 1);
        graphNode.addEdge(1, 4, 1);
        graphNode.addEdge(3, 7, 1);
        graphNode.addEdge(4, 7, 1);
        graphNode.addEdge(2, 5, 1);
        graphNode.addEdge(2, 6, 1);
        graphNode.addEdge(5, 6, 1);
        graphNode.showGraph();

        graphNode.dfs();
        graphNode.bfs();

    }

}

class GraphNode {
    int n;
    ArrayList<String> vertex;
    int[][] weight;
    boolean[] isVisited;


    public GraphNode(int n) {
        this.n = n;
        vertex = new ArrayList<>();
        weight = new int[n][n];
    }

    public void addVertex(String[] v) {
        for (String ver : v) {
            vertex.add(ver);
        }
    }

    public void addEdge(int v1, int v2, int w) {
        weight[v1][v2] = w;
        weight[v2][v1] = w;
    }

    public void showGraph() {
        for (int[] link : weight) {
            System.out.println(Arrays.toString(link));
        }
    }

    public int getFirst(int index) {
        for (int j = 0; j < vertex.size(); j++) {
            if (weight[index][j] > 0) {
                return j;
            }
        }
        return -1;
    }

    public int getNext(int v1, int v2) {
        for (int j = v2 + 1; j < vertex.size(); j++) {
            if (weight[v1][j] > 0) {
                return j;
            }
        }
        return -1;
    }

    public void dfs(int i) {
        String verValue = vertex.get(i);
        System.out.print(verValue + "->");
        isVisited[i] = true;
        int w = -1;
        for (int j = 0; j < vertex.size(); j++) {
            if (weight[i][j] > 0) {
                w = j;
                break;
            }
        }
        while (w != -1) {
            if (!isVisited[w]) {
                dfs(w);
            }
            w = getNext(i, w);
        }
    }

    public void dfs() {
        int n = vertex.size();
        isVisited = new boolean[n];
        for (int i = 0; i < n; i++) {
            if (!isVisited[i]) {
                dfs(i);
            }
        }
    }

    public void bfs(int i) {
        int u;
        int w;
        LinkedList queue = new LinkedList();
        System.out.println();
        System.out.print(vertex.get(i) + "->");
        isVisited[i] = true;
        queue.addLast(i);
        while (!queue.isEmpty()) {
            u = (Integer) queue.removeFirst();
            w = getFirst(u);
            while (w != -1) {
                if (!isVisited[w]) {
                    System.out.print(vertex.get(w) + "->");
                    isVisited[w] = true;
                    queue.addLast(w);
                }
                w = getNext(u, w);
            }
        }
    }

    public void bfs() {
        isVisited = new boolean[vertex.size()];
        for (int i = 0; i < vertex.size(); i++) {
            if (!isVisited[i]) {
                bfs(i);
            }
        }
    }

//    int n;
//    int[] vertex;
//    int[][] weight;
//
//    public GraphNode(int n) {
//        this.n = n;
//        vertex = new int[n];
//        weight = new int[n][n];
//    }

//    public void createGraph(int n, int[] vertex, int[][] weight, GraphNode graphNode) {
//        for (int i = 0; i < n; i++) {
//            graphNode.vertex[i] = vertex[i];
//            for (int j = 0; j < n; j++) {
//                graphNode.weight[i][j] = weight[i][j];
//            }
//        }
//    }
//
//    public void showGraph(int[][] weight){
//        for (int[] link: weight){
//            System.out.println(Arrays.toString(link));
//        }
//    }
}