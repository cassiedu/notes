package Test;

public class BubbleSort {
    public static void main(String[] args) {
        int[] edge = {2, 3, 4, 1};
        Sort sort = new Sort();
        sort.sortWeight(edge);
    }
}

class Sort {
    int temp;

    public void sortWeight(int[] edge) {
        for (int i = 0; i < edge.length - 1; i++) {
            for (int j = 0; j < edge.length - 1 - i; j++) {
                if (edge[j] > edge[j + 1]) {
                    temp = edge[j];
                    edge[j] = edge[j + 1];
                    edge[j + 1] = temp;
                }
            }
        }
    }
}
