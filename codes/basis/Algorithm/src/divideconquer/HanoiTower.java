package divideconquer;

public class HanoiTower {
    public static void main(String[] args) {
        hanoiTower(2, "A", "B", "C");
    }

    public static void hanoiTower(int num, String a, String b, String c) {
        if (num == 1) {
            System.out.println("将第" + num + "个盘子" + "从" + a + "移动到" + c);
        } else {
            hanoiTower(num - 1, a, c, b); //1.A上面的盘移动到B，其中使用到C
            System.out.println("将第" + num + "个盘子" + "从" + a + "移动到" + c); //2.A下面的盘移动到C
            hanoiTower(num - 1, b, a, c); //3.B->C,会使用到A
        }
    }
}
