package prim;

import java.util.Arrays;

public class Prim {
    public static void main(String[] args) {
        char[] city = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        int[][] distance = new int[][]{
                {10000, 5, 7, 10000, 10000, 10000, 2},
                {5, 10000, 10000, 9, 10000, 10000, 3},
                {7, 10000, 10000, 10000, 8, 10000, 10000},
                {10000, 9, 10000, 10000, 10000, 4, 10000},
                {10000, 10000, 8, 10000, 10000, 5, 4},
                {10000, 10000, 10000, 4, 5, 10000, 6},
                {2, 3, 10000, 10000, 4, 6, 10000},};
        int n = city.length;
        MinSpanTree minSpanTree = new MinSpanTree();
        Graph graph = new Graph(n);
        minSpanTree.createGraph(graph, city, distance, n);
        minSpanTree.showGraph(graph);
        minSpanTree.prim(graph, 0);
    }
}

class MinSpanTree {
    //把数据存储到图中
    public void createGraph(Graph graph, char[] city, int[][] distance, int n) {
        for (int i = 0; i < n; i++) {
            graph.city[i] = city[i];
            for (int j = 0; j < n; j++) {
                graph.distance[i][j] = distance[i][j];
            }
        }
    }

    public void showGraph(Graph graph) {
        for (int[] link : graph.distance) {
            System.out.println(Arrays.toString(link));
        }
    }

    public void prim(Graph graph, int v) {
        int numVertex = graph.n;
        int visited[] = new int[numVertex];
        for (int i = 0; i < numVertex; i++) {
            visited[i] = 0;
        }
        visited[v] = 1;
        int h1 = -1;
        int h2 = -1;
        int minWeight = 10000;
        for (int k = 1; k < numVertex; k++) {
            for (int i = 0; i < numVertex; i++) {
                for (int j = 0; j < numVertex; j++) {
                    if (visited[i] == 1 && visited[j] == 0 && graph.distance[i][j] < minWeight) {
                        minWeight = graph.distance[i][j];
                        h1 = i;
                        h2 = j;
                    }
                }
            }
            System.out.println("边<" + graph.city[h1] + "," + graph.city[h2] + "> 权值:" + minWeight);
            visited[h2] = 1;
            minWeight = 10000;
        }
    }
}

class Graph {
    int n;
    char[] city;
    int[][] distance;

    public Graph(int n) {
        this.n = n;
        city = new char[n];
        distance = new int[n][n];
    }
}
