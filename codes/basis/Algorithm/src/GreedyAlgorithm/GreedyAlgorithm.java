package GreedyAlgorithm;

import java.util.HashMap;
import java.util.HashSet;

public class GreedyAlgorithm {
    public static void main(String[] args) {
        HashSet<String> towerArea1 = new HashSet<>();
        towerArea1.add("Beijing");
        towerArea1.add("shanghai");
        towerArea1.add("tianjin");

        HashSet<String> towerArea2 = new HashSet<>();
        towerArea2.add("guangzhou");
        towerArea2.add("beijing");
        towerArea2.add("shenzhen");

        HashSet<String> towerArea3 = new HashSet<>();
        towerArea3.add("chengdu");
        towerArea3.add("shanghai");
        towerArea3.add("hangzhou");

        HashSet<String> towerArea4 = new HashSet<>();
        towerArea4.add("shanghai");
        towerArea4.add("tianjin");

        HashSet<String> towerArea5 = new HashSet<>();
        towerArea5.add("hangzhou");
        towerArea5.add("dalian");

        HashMap<String, HashSet<String>> towerAll = new HashMap<>();
        towerAll.put("K1", towerArea1);
        towerAll.put("K2", towerArea2);
        towerAll.put("K3", towerArea3);
        towerAll.put("K4", towerArea4);
        towerAll.put("K5", towerArea5);

        HashSet<String> areaAll = new HashSet<>();
        areaAll.add("Beijing");
        areaAll.add("shanghai");
        areaAll.add("tianjin");
        areaAll.add("guangzhou");
        areaAll.add("shenzhen");
        areaAll.add("chengdu");
        areaAll.add("hangzhou");
        areaAll.add("dalian");

        String maxKey = null;
        HashSet<String> tempTower = new HashSet<>();
        HashSet<String> result = new HashSet<>();
        while (areaAll.size() > 0) {
            for (String key : towerAll.keySet()) {
                HashSet<String> tower = towerAll.get(key);
                tempTower.addAll(tower);
                tempTower.retainAll(areaAll);
                if (tempTower.size() > 0 && (maxKey == null || tempTower.size() > towerAll.get(maxKey).size())) {
                    maxKey = key;
                }
                tempTower.clear();
            }
            if (maxKey != null) {
                result.add(maxKey);
                System.out.println(result);
                areaAll.removeAll(towerAll.get(maxKey));
            }
            maxKey = null;
        }


    }


}
