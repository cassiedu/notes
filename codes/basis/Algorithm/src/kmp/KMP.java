package kmp;

import java.util.Arrays;

public class KMP {
    public static void main(String[] args) {
        String str1 = "BBC ABCDAB ABCDABCDABDE";
        String str2 = "ABCDABD";

        int[] match = matchVal(str2);
        ; //[0, 1, 2, 0]
        System.out.println("next=" + Arrays.toString(match));

        index(str1, str2, match);
    }

    public static int index(String str1, String str2, int[] match) {
        for (int i = 0, j = 0; i < str1.length(); i++) {
            if (str1.charAt(i) == str2.charAt(j)) {
                j++;
            }
            if (j == str2.length()) {
                return i - j + 1;
            }
            while (j > 0 && str1.charAt(i) != str2.charAt(j)) {
                j = match[j - 1];
            }
        }
        return -1;
    }

    public static int[] matchVal(String str) {
        int[] match = new int[str.length()];
        match[0] = 0;
        for (int i = 1, j = 0; i < str.length(); i++) {
            while (j > 0 && str.charAt(i) != str.charAt(j)) {
                j = match[j - 1];
            }
            if (str.charAt(i) == str.charAt(j)) {
                j++;
            }
            match[i] = j;
        }
        return match;
    }

}
