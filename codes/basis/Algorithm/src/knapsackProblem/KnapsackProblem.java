package knapsackProblem;

public class KnapsackProblem {
    public static void main(String[] args) {
        int[] w = {1, 4, 3};
        int[] v = {1500, 3000, 2000};
        int capacity = 4;
        int num = w.length;
        //val只与背包的容量和背包可以装的物品数量有关；与有几个物品无关
        //这里用num是因为允许背包装的物品数目=所有的物品数目
        int[][] val = new int[num + 1][capacity + 1];
        int[][] flag = new int[num + 1][capacity + 1];

        for (int i = 1; i < num + 1; i++) {
            for (int j = 1; j < capacity + 1; j++) {
                if (w[i - 1] > j) { //注意>j不是capacity
                    val[i][j] = val[i - 1][j];
                } else {
                    //注意val[][]第一行第一列是0
                    //注意两个表，分清i-1在已知信息的重量表表示当前；在需要构建的价值表表示前一个
                    if (val[i - 1][j] < v[i - 1] + val[i - 1][j - w[i - 1]]) {
                        val[i][j] = v[i - 1] + val[i - 1][j - w[i - 1]];
                        flag[i][j] = 1;
                    } else {
                        val[i][j] = val[i - 1][j];
                    }
                }
            }
        }

        for (int i = 0; i < num + 1; i++) {
            for (int j = 0; j < capacity + 1; j++) {
                System.out.printf(val[i][j] + "\t");
            }
            System.out.println();
        }

        while (num > 0 && capacity > 0) { //从path的最后开始找
            if (flag[num][capacity] == 1) {
                System.out.printf("第%d个商品放入到背包\n", num);
                capacity -= w[num - 1]; //w[i-1]
            }
            num--;
        }
    }
}
