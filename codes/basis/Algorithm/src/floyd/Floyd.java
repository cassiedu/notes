package floyd;

import java.util.Arrays;

public class Floyd {
    public static void main(String[] args) {
        char[] vertex = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        int n = vertex.length;
        int[][] matrix = new int[n][n];
        final int N = 65535;
        matrix[0] = new int[]{0, 5, 7, N, N, N, 2};
        matrix[1] = new int[]{5, 0, N, 9, N, N, 3};
        matrix[2] = new int[]{7, N, 0, N, 8, N, N};
        matrix[3] = new int[]{N, 9, N, 0, N, 4, N};
        matrix[4] = new int[]{N, N, 8, N, 0, 5, 4};
        matrix[5] = new int[]{N, N, N, 4, 5, 0, 6};
        matrix[6] = new int[]{2, 3, N, N, 4, 6, 0};

        Graph graph = new Graph(n, matrix, vertex);
        graph.floyd();
        graph.show();
    }
}

class GraphFloyd {
    private char[] vertex;
    private int[][] dis;
    private int[][] pre;
    int n = vertex.length;

    public GraphFloyd(int n, int[][] weight, char[] vertex) {
        this.vertex = vertex;
        this.dis = weight; //各个顶点出发到其他顶点的距离
        this.pre = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(pre[i], i);
        }
    }

    public void show() {
        char[] vertex = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                System.out.print(vertex[pre[k][i]] + "\t");
            }
            System.out.println();
            for (int i = 0; i < n; i++) {
                System.out.print(vertex[k] + "->" + vertex[i] + ": " + dis[k][i]);
            }
            System.out.println();
            System.out.println();
        }
    }

    public void floyd() {
        int disSum;
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    disSum = dis[i][k] + dis[k][j];
                    if (disSum < dis[i][j]) {
                        dis[i][j] = disSum;
                        pre[i][j] = pre[k][j];
                    }
                }
            }
        }
    }
}
