package horsechess;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;

public class HorseChess {
    private static int X; // 棋盘的列数
    private static int Y; // 棋盘的行数
    //创建一个数组，标记棋盘的各个位置是否被访问过
    private static boolean isVisited[];
    //使用一个属性，标记是否棋盘的所有位置都被访问
    private static boolean finished; // 如果为true,表示成功

    public static void main(String[] args) {

    }

//    public static void sort(ArrayList<Point> ps) {
//        ps.sort(new Comparator<Point>() {
//
//            @Override
//            public int compare(Point o1, Point o2) {
//                // TODO Auto-generated method stub
//                //获取到o1的下一步的所有位置个数
//                int count1 = next(o1).size();
//                //获取到o2的下一步的所有位置个数
//                int count2 = next(o2).size();
//                if(count1 < count2) {
//                    return -1;
//                } else if (count1 == count2) {
//                    return 0;
//                } else {
//                    return 1;
//                }
//            }
//
//        });
//    }

    public static void traversal(int[][] chessBoard, int row, int col, int step) {
        chessBoard[row][col] = step;
        isVisited[row * X + col] = true;
        ArrayList<Point> ps = next(new Point(col, row));
        while (!ps.isEmpty()) {
            Point p = ps.remove(0);
            if (!isVisited[p.y * X + p.x]) {
                traversal(chessBoard, p.y, p.x, step + 1);
            }
        }
        if (step < X * Y && !finished) {
            chessBoard[row][col] = 0;
            isVisited[row * X + col] = false;
        } else {
            finished = true;
        }
    }

    public static ArrayList<Point> next(Point curPoint) {
        ArrayList<Point> ps = new ArrayList<>();
        Point p1 = new Point();
        if ((p1.x = curPoint.x - 2) >= 0 && (p1.y = curPoint.y - 1) >= 0) {
            ps.add(p1);
        }
        if ((p1.x = curPoint.x - 1) >= 0 && (p1.y = curPoint.y - 2) >= 0) {
            ps.add(p1);
        }
        //判断马儿可以走7这个位置
        if ((p1.x = curPoint.x + 1) < X && (p1.y = curPoint.y - 2) >= 0) {
            ps.add(new Point(p1));
        }
        //判断马儿可以走0这个位置
        if ((p1.x = curPoint.x + 2) < X && (p1.y = curPoint.y - 1) >= 0) {
            ps.add(new Point(p1));
        }
        //判断马儿可以走1这个位置
        if ((p1.x = curPoint.x + 2) < X && (p1.y = curPoint.y + 1) < Y) {
            ps.add(new Point(p1));
        }
        //判断马儿可以走2这个位置
        if ((p1.x = curPoint.x + 1) < X && (p1.y = curPoint.y + 2) < Y) {
            ps.add(new Point(p1));
        }
        //判断马儿可以走3这个位置
        if ((p1.x = curPoint.x - 1) >= 0 && (p1.y = curPoint.y + 2) < Y) {
            ps.add(new Point(p1));
        }
        //判断马儿可以走4这个位置
        if ((p1.x = curPoint.x - 2) >= 0 && (p1.y = curPoint.y + 1) < Y) {
            ps.add(new Point(p1));
        }
        return ps;
    }

}
