package dynamic;

import java.util.List;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2021年02月20日<br>
 * @see dynamic <br>
 */
public class DPOptSpace {
    public static void main(String[] args) {

    }

    public int minimumTotal(List<List<Integer>> triangle) {
        int n = triangle.size();
        int[] res = new int[n];
        for (int m = 0; m < n; m++) {
            res[m] = triangle.get(n-1).get(m);
        }
        for (int i = triangle.size()-2; i >= 0; i--) {
            List<Integer> row = triangle.get(i);
            for (int j = 0; j < i + 1; j++) {
                res[j] = Math.min(res[j], res[j+1]) + row.get(j);
            }
        }
        return res[0];
    }
}
