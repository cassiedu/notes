package dynamic;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2021年02月20日<br>
 * @see dynamic <br>
 */
public class DP {
    public static void main(String[] args) {

    }
    //矩阵类
    public int climbStars(int n) {
        if(n == 1) return 1;
        int[] dp = new int[n+1];
        dp[0] = 0; dp[1] = 1; dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }

    public int minimumTotal(List<List<Integer>> triangle) {
        int n = triangle.size();
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++){
            dp[n-1][i] = triangle.get(n-1).get(i);
        }
        for (int j = n-2; j >= 0; j--){
            for (int k = 0; k < j+1; k++) {
                dp[j][k] = Math.min(dp[j+1][k], dp[j+1][k+1]) + triangle.get(j).get(k);
            }
        }
        return dp[0][0];
    }

    public int maxSubArray(int[] nums) {
        if(nums == null || nums.length ==0) return 0;
        int res = nums[0];
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(dp[i-1], 0) + nums[i];
            res = Math.max(res, dp[i]);
        }
        return res;
    }

    //数组类
    public int longestSubList(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n];
        int res = 0;
        Arrays.fill(dp, 1);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]){
                    dp[i] = Math.max(dp[j] + 1, dp[i]);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }

    public int minPaintCost(int[][] costs) {
        if (costs == null || costs.length == 0 || costs[0].length == 0) return 0;
        int r = costs.length;
        int[][] dp = new int[r][3];
        for (int i = 0; i < 3; i++) {
            dp[0][i] = costs[0][i];
        }
        for (int i = 1; i < r; i++) {
            dp[i][0] = Math.min(costs[i-1][1], costs[i-1][2]) + costs[i][0];
            dp[i][1] = Math.min(costs[i-1][0], costs[i-1][2]) + costs[i][1];
            dp[i][2] = Math.min(costs[i-1][0], costs[i-1][1]) + costs[i][2];
        }
        return Math.min(dp[r-1][0], Math.min(dp[r-1][1], dp[r-1][2]));
    }

    public int minPaintCostII(int[][] costs) {
        if (costs == null || costs.length == 0 || costs[0].length == 0) return 0;
        int r = costs.length;
        int c = costs[0].length;
        int[][] dp = new int[r][c];
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < c; i++) {
            dp[0][i] = costs[0][i];
        }
        for (int i = 0; i < c; i++) {
            dp[1][i] = Integer.MAX_VALUE;
        }
        for (int i = 1 ; i < r; i++) {
            int minIndex = 0;
            int min1 = Integer.MAX_VALUE;
            int min2 = min1;
            for (int j = 1; j < c; j++) {
                if (dp[i-1][j] < min1) {
                    min2 = min1;
                    min1 = dp[i-1][j];
                    minIndex = j;
                } else if(dp[i-1][j] < min2){
                    min2 = dp[i-1][j];
                }
            }
            for (int j = 1; j < c; j++) {
                if (j != minIndex) dp[i][j] = Math.min(min1 + costs[i][j], dp[i][j]);
                else dp[i][j] = Math.min(min2 + costs[i][j], dp[i][j]);
            }
        }
        for (int i = 0; i < c; i++) {
            res = Math.min(res, dp[r-1][i]);
        }
        return res;
    }

    public int robHouse(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n+1];
        dp[1] = nums[0];
        for (int i = 2; i < n+1; i++) {
            dp[i] = Math.max(dp[i-1], dp[i-2] + dp[i]);
        }
        return dp[n];
    }

    //背包问题
    public int zeroOnePack(int V, int[] C, int[] W) {
        if(V <= 0 || C == null || C.length == 0 || W == null || C.length != W.length) return 0;
        int n = C.length;
        int[][] dp = new int[n+1][V+1];
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= V; j++) {
                if (j-C[i-1] >= 0) dp[i][j] = Math.max(dp[i-1][j], dp[i-1][j-C[i-1]] + W[i-1]);
            }
        }
        return dp[n][V];
    }

    public int zeroOneOpt(int V, int[] C, int[] W) {
        if(C == null || C.length == 0 || W == null || C.length != W.length) return 0;
        int n = C.length;
        int[] dp = new int[V+1];
        for (int i = 0; i < n; i++) {
            for (int j = V; j >= C[i]; j--) {
                dp[j] = Math.max(dp[j-1], dp[j-C[i]] + W[i]);
            }
        }
        return dp[V];
    }

    public boolean canPartition(int[] nums) {
        if (nums == null || nums.length == 0) return false;
        int n = nums.length;
        int sum = 0;
        int max = 0;
        for (int num: nums) {
            sum += num;
            max = Math.max(max, num);
        }
        if ((sum&1) == 1) return false;
        sum /= 2;
        if (max > sum) return false;
        boolean[][] dp = new boolean[n][sum+1];
        for (int i = 0; i < n; i++) {
            dp[i][0] = true;
        }
        dp[0][nums[0]] = true;
        for (int i = 1; i < n; i++) {
            for (int j = 1; j <= sum; j++) {
                if (j > nums[i]) dp[i][j] = dp[i-1][j] |= dp[i-1][j-nums[i]];
                else dp[i][j] = dp[i-1][j];
            }
        }
        return dp[n-1][sum];
    }

    public boolean canPartitionOpt(int[] nums) {
        if (nums == null || nums.length == 0) return false;
        int n = nums.length;
        int sum = 0;
        int max = 0;
        for (int num: nums) {
            sum += num;
            max = Math.max(max, num);
        }
        if ((sum&1) == 1) return false;
        sum /= 2;
        if (max > sum) return false;
        boolean[] dp = new boolean[sum+1];
        dp[0] = true;
        for (int i = 0; i < n; i++) {
            for (int j = sum; j >= nums[i]; j--) {
                dp[j] |= dp[j-nums[i]];
            }
        }
        return dp[sum];
    }

    public int coinChange(int[] coins, int amount) {
        if (coins == null || coins.length == 0 || amount == 0) return 0;
        int n = coins.length;
        int[] dp = new int[n+1];
        Arrays.fill(dp, Integer.MAX_VALUE);
        dp[0] = 0;
        for (int i = 0; i < n; i++) {
            for (int j = coins[i]; j <= amount; ++j) {
                if (dp[j - coins[i]] != Integer.MAX_VALUE) {
                    dp[j] = Math.min(dp[j - coins[i]] + 1, dp[j]);
                }
            }
        }
        return dp[amount] == Integer.MAX_VALUE ? -1 : dp[amount];
    }

    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int i = 0; i < coins.length; ++i) {
            for (int j = coins[i]; j <= amount; ++j) {
                dp[j] += dp[j - coins[i]];
            }
        }

        return dp[amount];
    }
}
