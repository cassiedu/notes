package solution1;

import java.util.*;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2021年03月24日<br>
 * @see solution1 <br>
 */
public class Test {
    public static void main(String[] args) {

    }

    public int maxProfit (int[] prices) {
        // write code here
        if(prices == null || prices.length < 2) return 0;
        int n = prices.length;
        int[] dp = new int[n];
        dp[0] = 0;
        int min = prices[0];
        for(int i = 1; i < n; i++) {
            dp[i] = Math.max(prices[i] - min, dp[i-1]);
            min = prices[i] < min ? prices[i]: min;
        }
        return dp[n];
    }
}
