package solution;

import java.util.*;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2021年03月16日<br>
 * @see solution <br>
 */
class TreeClass {
    public static void main(String[] args) {
        String line = "[2]";
        TreeNode root = new Create().stringToTreeNode(line);

    }
}

class Codec {
    public String rserialize(TreeNode root, String str) {
        if (root == null) {
            str += "None,";
        } else {
            str += str.valueOf(root.val) + ",";
            str = rserialize(root.left, str);
            str = rserialize(root.right, str);
        }
        System.out.println("str: " + str);
        return str;
    }

    public String serialize(TreeNode root) {
        return rserialize(root, "");
    }

    public TreeNode rdeserialize(List<String> l) {
        if (l.get(0).equals("None")) {
            l.remove(0);
            return null;
        }

        TreeNode root = new TreeNode(Integer.valueOf(l.get(0)));
        l.remove(0);
        root.left = rdeserialize(l);
        root.right = rdeserialize(l);

        return root;
    }

    public TreeNode deserialize(String data) {
        String[] data_array = data.split(",");
        List<String> data_list = new LinkedList<String>(Arrays.asList(data_array));
        return rdeserialize(data_list);
    }
}

//最近祖先
class LowestParent {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null || root == p || root == q) return root;
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if(left == null) return right;
        if(right == null) return left;
        return root;
    }


    Map<Integer, TreeNode> parent = new HashMap<>();
    Set<Integer> visited = new HashSet<>();

    void dfs(TreeNode root) {
        if (root.left != null) {
            parent.put(root.left.val, root);
            dfs(root.left);
        }
        if (root.right != null) {
            parent.put(root.right.val, root);
            dfs(root.right);
        }
    }

    public TreeNode lowestCommonAncestor1(TreeNode root, TreeNode p, TreeNode q) {
        dfs(root);
        while (p != null) {
            visited.add(p.val);
            p = parent.get(p.val);
        }
        while (q != null) {
            if (visited.contains(q.val)) {
                return q;
            }
            q = parent.get(q.val);
        }
        return null;
    }
}

//初始化节点
class TreeNode{
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }
}

//创建树节点
class Create{
    Deque<Integer> deque = new LinkedList<>();
    TreeNode stringToTreeNode(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return null;
        }

        String[] parts = input.split(",");
        String item = parts[0];
        TreeNode root = new TreeNode(Integer.parseInt(item));
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        nodeQueue.add(root);

        int index = 1;
        while(!nodeQueue.isEmpty()) {
            TreeNode node = nodeQueue.remove();

            if (index == parts.length) {
                break;
            }

            item = parts[index++];
            item = item.trim();
            if (!item.equals("null")) {
                int leftNumber = Integer.parseInt(item);
                node.left = new TreeNode(leftNumber);
                nodeQueue.add(node.left);
            }

            if (index == parts.length) {
                break;
            }

            item = parts[index++];
            item = item.trim();
            if (!item.equals("null")) {
                int rightNumber = Integer.parseInt(item);
                node.right = new TreeNode(rightNumber);
                nodeQueue.add(node.right);
            }
        }
        return root;
    }

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return buildTreeHelper(preorder, 0, preorder.length, inorder, 0, inorder.length);
    }

    private TreeNode buildTreeHelper(int[] preorder, int p_start, int p_end, int[] inorder, int i_start, int i_end) {
        // preorder 为空，直接返回 null
        if (p_start == p_end) {
            return null;
        }
        int root_val = preorder[p_start];
        TreeNode root = new TreeNode(root_val);
        //在中序遍历中找到根节点的位置
        int i_root_index = 0;
        for (int i = i_start; i < i_end; i++) {
            if (root_val == inorder[i]) {
                i_root_index = i;
                break;
            }
        }
        int leftNum = i_root_index - i_start;
        //递归的构造左子树
        root.left = buildTreeHelper(preorder, p_start + 1, p_start + leftNum + 1, inorder, i_start, i_root_index);
        //递归的构造右子树
        root.right = buildTreeHelper(preorder, p_start + leftNum + 1, p_end, inorder, i_root_index + 1, i_end);
        return root;
    }
}

//前中后的递归非递归遍历
class Order{
    void preOrder(TreeNode head){
        List<Integer> list = new ArrayList<>();
        if (head == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(head);
        while (!stack.isEmpty()) {
            TreeNode cur = stack.pop();
            list.add(cur.val);
            if (cur.right != null) {
                stack.push(cur.right);
            }
            if (cur.left != null) {
                stack.push(cur.left);
            }
        }
        System.out.println(Arrays.toString(list.toArray()));
    }

    public void inorder(TreeNode head) {
        List<Integer> list = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        while (head != null || !stack.isEmpty()) {
            while (head != null) {
                stack.add(head);
                head = head.left;
            }
            TreeNode node = stack.pop();
            list.add(node.val);
            head = node.right;
        }
    }

    public void postOrder(TreeNode head) {
        if (head == null) {
            return;
        }
        List<Integer> list = new LinkedList<>();
        Stack<TreeNode> stack = new Stack<>();
        stack.add(head);
        while (!stack.isEmpty()) {
            head = stack.pop();
            list.add(0,head.val);
            if (head.left != null) stack.push(head.left);
            if (head.right != null) stack.push(head.right);
        }
    }

    public void preOrderRecursion(TreeNode head) {
        List<Integer> list = new ArrayList<>();
        if (head == null) {
            return;
        }
        list.add(head.val);
        if (head.left != null) {
            preOrderRecursion(head.left);
        }
        if (head.right != null) {
            preOrderRecursion(head.right);
        }
        System.out.println(Arrays.toString(list.toArray()));
    }

    public void inOrderRecursion(TreeNode head) {
        List<Integer> list = new ArrayList<>();
        if (head == null) {
            return;
        }
        if (head.left != null) {
            inOrderRecursion(head.left);
        }
        list.add(head.val);
        if (head.right != null) {
            inOrderRecursion(head.right);
        }
        System.out.println(Arrays.toString(list.toArray()));
    }

    public void postOrderRecursion(TreeNode head) {
        List<Integer> list = new ArrayList<>();
        if (head == null) {
            return;
        }
        if (head.left != null) {
            postOrderRecursion(head.left);
        }
        if (head.right != null) {
            postOrderRecursion(head.right);
        }
        list.add(head.val);
        System.out.println(Arrays.toString(list.toArray()));
    }

    //锯齿形层次遍历
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> res = new LinkedList<>();
        if(root == null) return res;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        boolean leftFlag = true;
        while(!queue.isEmpty()) {
            Deque<Integer> deque = new LinkedList<>();
            int level = queue.size();
            while(level-- != 0) {
                TreeNode node = queue.poll();
                if(leftFlag) {
                    deque.offerLast(node.val);
                } else {
                    deque.offerFirst(node.val);
                }
                if(node.left != null) queue.offer(node.left);
                if(node.right != null) queue.offer(node.right);
            }
            leftFlag = !leftFlag;
            res.add(new LinkedList<Integer>(deque));
        }
        return res;
    }

    //判断两个树是否相同递归
    public boolean isSameTreeRecursion(TreeNode p, TreeNode q) {
        if(p == null && q == null) return true;
        if(p == null || q == null || p.val != q.val) return false;
        return isSameTreeRecursion(p.left, q.left) && isSameTreeRecursion(p.right, q.right);
    }

    //判断两个树是否相同非递归
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if((p == null && q != null) || (q == null && p != null)) return false;
        if(p == null && q == null) return true;
        Queue<TreeNode> queue1 = new LinkedList<>();
        queue1.offer(p);
        Queue<TreeNode> queue2 = new LinkedList<>();
        queue2.offer(q);
        while(!queue1.isEmpty()) {
            if(queue1.size() != queue2.size()) return false;
            int num = queue1.size();
            while(num > 0) {
                TreeNode node1 = queue1.poll();
                TreeNode node2 = queue2.poll();
                if(node1.val != node2.val || (node1.left == null && node2.left != null)
                        || (node1.left != null && node2.left == null)
                        || (node1.right == null && node2.right != null)
                        || (node1.right != null && node2.right == null)) return false;
                if(node1.left != null) {
                    queue1.add(node1.left);
                    queue2.add(node2.left);
                }
                if(node2.left != null) {
                    queue1.add(node1.right);
                    queue2.add(node2.right);
                }
                num--;
            }
        }
        return true;
    }

    //判断是否是平衡二叉树
    int dif = 0;
    public boolean isBalanced(TreeNode root) {
        height(root);
        return dif > 1 ? false: true;
    }
    public int height(TreeNode root) {
        if(root == null) return 0;
        int left = height(root.left) + 1;
        int right = height(root.right) + 1;
        dif = Math.max(dif, Math.abs(left-right));
        return Math.max(left, right);
    }

    //反转二叉树
    /**
     * 递归方式遍历反转
     */
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }

        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;

        invertTree(root.left);
        invertTree(root.right);
        return root;
    }

    /**
     * 层序遍历方式反转
     */
    public TreeNode invertTreeByQueue(TreeNode root) {
        if (root == null) {
            return null;
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            TreeNode temp = node.left;
            node.left = node.right;
            node.right = temp;
            if (node.left != null) {
                queue.offer(node.left);
            }
            if (node.right != null) {
                queue.offer(node.right);
            }
        }
        return root;
    }

    /**
     * 深度优先遍历的方式反转
     */
    private TreeNode invertTreeByStack(TreeNode root) {
        if (root == null) {
            return null;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            int size = stack.size();
            for (int i = 0; i < size; i++) {
                TreeNode cur = stack.pop();
                TreeNode temp = cur.left;
                cur.left = cur.right;
                cur.right = temp;
                if (cur.right != null) {
                    stack.push(cur.right);
                }
                if (cur.left != null) {
                    stack.push(cur.left);
                }
            }
        }
        return root;
    }
}

//节点数量相关
class Count{
    //统计节点总数
    public int countNodes(TreeNode root) {
        if (root == null){
            return 0;
        }
        int left = countNodes(root.left);
        int right = countNodes(root.right);
        return left + right + 1;
    }

    //二叉树最大深度DFS
    public int maxDepth(TreeNode root) {
        if(root == null) return 0;
        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        return Math.max(left, right) + 1;
    }

    //二叉树最大深度BFS
    public int maxDepth1(TreeNode root) {
        if (root == null) return 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int res = 0;
        while(!queue.isEmpty()) {
            int size = queue.size(); //size每一层的节点数
            while(size > 0) {
                TreeNode node = queue.poll();
                if(node.left != null) queue.offer(node.left);
                if(node.right != null) queue.offer(node.right);
                size--;
            }
            res++;
        }
        return res;
    }

    //二叉树最小深度DFS
    public int minDepth(TreeNode root) {
        if(root == null) return 0;
        int left = minDepth(root.left) + 1;
        int right = minDepth(root.right) + 1;
        if(root.left == null && root.right != null) return right;
        else if(root.left != null && root.right == null) return left;
        else return Math.min(left, right);
    }

    //二叉树最小深度BFS
    public static int minDepth1(TreeNode root) {
        if (root == null) return 0;
        Deque<TreeNode> deque = new LinkedList<>();
        deque.offer(root);
        int level = 1;
        while (!deque.isEmpty()) {
            int size = deque.size();
            for (int i = 0; i < size; i++) {
                TreeNode cur = deque.poll();
                if (cur.right == null && cur.left == null) return level;
                if (cur.left != null) deque.offer(cur.left);
                if (cur.right != null) deque.offer(cur.right);
            }
            level++;
        }
        return level;
    }

    //二叉树直径：任意两节点的最长路径边数
    int ans = 1;
    public int diameterOfBinaryTree(TreeNode root) {
        depth(root);
        return ans - 1;
    }
    public int depth(TreeNode node) {
        if (node == null) {
            return 0; // 访问到空节点了，返回0
        }
        int L = depth(node.left); // 左儿子为根的子树的深度
        int R = depth(node.right); // 右儿子为根的子树的深度
        ans = Math.max(ans, L+R+1); // 计算d_node即L+R+1 并更新ans
        return Math.max(L, R) + 1; // 返回该节点为根的子树的深度
    }
}

//二叉树路径
class Path{
    //二叉树到叶子节点的所有路径
    List<String> res = new ArrayList<>();
    public List<String> binaryTreePaths(TreeNode root) {
        if(root==null) return res;
        dfs(root,new StringBuilder());
        return res;
    }
    public void dfs(TreeNode r,StringBuilder s){
        if(r==null) return;
        s.append(r.val);
        if(r.left==null && r.right==null){
            res.add(s.toString());
        }
        dfs(r.left,new StringBuilder(s).append("->"));
        dfs(r.right,new StringBuilder(s).append("->"));
    }

    //二叉树节点和等于固定值的到叶子节点的路径
    ArrayList<ArrayList<Integer>> res1 = new ArrayList<>();
    ArrayList<Integer> list = new ArrayList<>();

    ArrayList<ArrayList<Integer>> findTargetPath(TreeNode root,int target) {
        if(root == null) return res1;
        list.add(root.val);
        target -= root.val;
        if(target == 0 && root.left == null && root.right == null)
            res1.add(new ArrayList<>(list));//每个数组为一个新对象
        findTargetPath(root.left, target);
        findTargetPath(root.right, target);
        list.remove(list.size()-1);
        return res1;
    }


}
