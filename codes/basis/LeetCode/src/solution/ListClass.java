package solution;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2021年03月18日<br>
 * @see solution <br>
 */
public class ListClass {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        String input = "[2]";
        ListNode head = MainClass.stringToListNode(input);
        String input1 = "[1]";
        ListNode head1 = MainClass.stringToListNode(input1);
        ListNode res = Traverse.class.newInstance().mergeTwoLists(head, head1);
        ((Traverse) Class.forName("solution.Traverse").newInstance()).printList(res);
//        System.exit(0);
    }
}

class Traverse{


    //打印链表
    public void printList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
    }
    //反转链表
    public ListNode reverseList(ListNode head) {
        if(head == null) return null;
        Stack<ListNode> stack = new Stack<>();
        while(head != null) {
            stack.push(head);
            head = head.next;
        }
        ListNode vir = stack.pop();
        ListNode node = vir;
        while(!stack.isEmpty()) {
            node.next = stack.pop();
            node = node.next;
            System.out.println(node.val);
        }
        return vir;
    }

    public ListNode reverseListRecursion(ListNode head) {
        //递归终止条件是当前为空，或者下一个节点为空
        if(head==null || head.next==null) {
            return head;
        }
        //这里的cur就是最后一个节点
        ListNode cur = reverseListRecursion(head.next);
        //这里请配合动画演示理解
        //如果链表是 1->2->3->4->5，那么此时的cur就是5
        //而head是4，head的下一个是5，下下一个是空
        //所以head.next.next 就是5->4
        head.next.next = head;
        //防止链表循环，需要将head.next设置为空
        head.next = null;
        //每层递归函数都返回cur，也就是最后一个节点
        return cur;
    }

    //链表有环
    public boolean hasCycle(ListNode head) {
        Set<ListNode> seen = new HashSet<>();
        while (head != null) {
            boolean temp = seen.add(head);
            if (!temp) {
                return true;
            }
            head = head.next;
        }
        return false;
    }

    //链表排序
    public ListNode sortList(ListNode head) {
        if(head == null || head.next == null) return head;
        // 2、找到链表中间节点并断开链表 & 递归下探
        ListNode midNode = middleNode(head);
        ListNode rightHead = midNode.next;
        midNode.next = null;
        ListNode left = sortList(head);
        ListNode right = sortList(rightHead);
        // 3、当前层业务操作（合并有序链表）
        return mergeLists(left, right);
    }

    public ListNode middleNode(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode slow = head, fast = head.next.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    //合并两个有序链表
    public ListNode mergeTwoLists (ListNode l1, ListNode l2) {
        // write code here
        if(l1 == null) return l2;
        else if(l2 == null) return l1;
        ListNode pre = new ListNode(-1), head;
        head = l1.val < l2.val ? l1: l2;
        pre.next = head;
        while(l1 != null && l2 != null) {
            while(l2 != null && l1.val >= l2.val) {
                l2 = l2.next;
                pre = pre.next;
            }
            pre.next = l1;
            if(l2 == null) return head;
            while(l1 != null && l1.val <= l2.val) {
                l1 = l1.next;
                pre  = pre.next;
            }
            pre.next = l2;
            if(l1 == null) return head;
        }
        return head;
    }

    public ListNode mergeLists(ListNode l1, ListNode l2) {
        ListNode vir = new ListNode(-1);
        ListNode cur = vir;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                cur.next = l1;
                l1 = l1.next;
            } else {
                cur.next = l2;
                l2 = l2.next;
            }
            cur = cur.next;
        }
        cur.next = l1 == null ? l2 : l1;
        return vir.next;
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode(){

    }
    ListNode(int val) {
        this.val = val;
    }
}

class MainClass {
    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static ListNode stringToListNode(String input) {
        // Generate array from the input
        int[] nodeValues = stringToIntegerArray(input);

        // Now convert that list into linked list
        ListNode dummyRoot = new ListNode(0);
        ListNode ptr = dummyRoot;
        for(int item : nodeValues) {
            ptr.next = new ListNode(item);
            ptr = ptr.next;
        }
        return dummyRoot.next;
    }
}
