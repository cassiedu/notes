package solution;

import java.util.*;

/**
Description :
 *
@author duyuzhu<br>
@version 1.0<br>
@taskId <br>
@CreateDate 2021年03月25日<br>
@see solution <br>
 */
public class ArrayClass {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, null, 2, null,3));
        int[] arr = new int[]{-2,3,0,1,2};
        new Solution().maxProduct(arr);
    }
}

class Solution {
    public int maxProduct(int[] nums) {
        int max = Integer.MIN_VALUE, imax = 1, imin = 1;
        for(int i=0; i<nums.length; i++){
            if(nums[i] < 0){
                int tmp = imax;
                imax = imin;
                imin = tmp;
            }
            imax = Math.max(imax*nums[i], nums[i]);
            imin = Math.min(imin*nums[i], nums[i]);

            max = Math.max(max, imax);
        }
        return max;
    }

    public ArrayList<ArrayList<Integer>> threeSum(int[] num) {
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        if(num == null || num.length < 3) return res;
        Arrays.sort(num);
        for(int i = 0; i < num.length - 2; i++) {
            if(num[i] > 0) return res;
            if(i > 0 && num[i] == num[i-1]) continue;
            int left = i+1, right = num.length-1;
            while(left < right) {
                int sum = num[i] + num[left] + num[right];
                if(sum == 0) {
                    res.add(new ArrayList<Integer>(Arrays.asList(num[i], num[left], num[right])));
                    while(left < right && num[left] == num[left+1]) left++;
                    while(left < right && num[right] == num[right-1]) right--;
                    left++;
                    right--;
                }else if(sum > 0) {
                    right--;
                } else {
                    left++;
                }
            }
        }
        return res;
    }

    public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
        intervals.sort((a, b) -> (a.start - b.start));
        Collections.sort(intervals, new Comparator<Interval>() {
            public int compare(Interval o1, Interval o2) {
                return o1.start-o2.start;
            }
        });
        for (int i = 1; i < intervals.size(); i++) {
            int preStart = intervals.get(i - 1).start;
            int preEnd = intervals.get(i - 1).end;
            int curStart = intervals.get(i).start;
            int curEnd = intervals.get(i).end;
            if (curStart <= preEnd) {
                intervals.set(i, new Interval(preStart, Math.max(preEnd, curEnd)));
                intervals.set(i - 1, null);
            }
        }

        while (intervals.remove(null)) ;
        return intervals;
    }
}
class Interval {
    int start;
    int end;
    Interval() { start = 0; end = 0; }
    Interval(int s, int e) { start = s; end = e; }
}

