package solution;

import java.util.Arrays;

class Reflection {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        int k = 2;
        double[] res1;
        double[] res2;
        double[] res3;

        Class c1 = A.class;
        A a = new A();
        Class c2 = a.getClass();
        Class c3 = Class.forName("solution.A");


        A a1 = (A)c1.newInstance();
        A a2 = (A)c2.newInstance();
        A a3 = (A)c3.newInstance();

        System.out.println(a1.lastRemaining(3,2));

        res1 = a1.dicesProbability(k);
        res2 = a2.dicesProbability(k);
        res3 = a3.dicesProbability(k);

        System.out.println("c1: " + c1 + " a1: " + a1 + " res1: " + Arrays.toString(res1));
        System.out.println("c2: " + c2 + " a2: " + a2 + " res2: " + Arrays.toString(res2));
        System.out.println("c3: " + c3 + " a3: " + a3 + " res3: " + Arrays.toString(res3));
    }

}

class A{
    public int lastRemaining(int n, int m) {
        return f(n, m);
    }

    public int f(int n, int m) {
        if (n == 1) {
            return 0;
        }
        int x = f(n - 1, m);
        System.out.println(x);
        return (m + x) % n;
    }

    public double[] dicesProbability(int n) {
        double[] res = new double[6*n-n+1];
        int[] dp = new int[6*n+1];
        for(int i = 1; i <= 6; i++){
            dp[i] = 1;
        }
        for(int i = 2; i <= n; i++) {
            for(int j = 6*i; j >= i; j--) {
                dp[j] = 0;
                for(int k = 1; k <= 6 && k < j; k++) {
                    if(j-k < i-1) break;
                    dp[j] += dp[j-k];
                }
            }
        }
        for(int i = n; i < res.length+n; i++) {
            res[i-n] = (double)(dp[i]/Math.pow(6,n));
        }
        return res;
    }
}
