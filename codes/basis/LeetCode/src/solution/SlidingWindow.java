package solution;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Description :
 *
 * @author duyuzhu<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2021年03月23日<br>
 * @see solution <br>
 */
public class SlidingWindow {
    public static void main(String[] args) {
        int[] nums = new int[]{-7,-8,7,5,7,1,6,0};
        new SlidingWindow().maxSlidingWindow(nums, 3);
    }

    public int[] maxSlidingWindow(int[] nums, int k) {
        int[] res = new int[nums.length-k+1];
        if(k > nums.length) return res;
        Deque<Integer> deque = new LinkedList<>();
        for(int i = 0; i < k ; i++) {
            while(!deque.isEmpty() && deque.peekLast() <= nums[i]) {
                deque.pollLast();
            }
            deque.offerLast(nums[i]);
        }
        res[0] = deque.peekFirst();
        for(int i = k; i < nums.length; i++) {
            if(deque.peekFirst() == nums[i-k]) {
                deque.pollFirst();
            }
            while(!deque.isEmpty() && deque.peekLast() < nums[i]) {
                deque.pollLast();
            }
            deque.offerLast(nums[i]);
            res[i-k+1] = deque.peekFirst();
        }
        return res;
    }
}
