package adapter.interfaceadapter;

public class Phone {
    public void charging(Vol5 vol5) {
        if (vol5.outputVol() == 5) {
            System.out.println("outputVol = 5, charging");
        } else if (vol5.outputVol() > 5) {
            System.out.println("outputVol > 5, can't charging");
        }
    }
}
