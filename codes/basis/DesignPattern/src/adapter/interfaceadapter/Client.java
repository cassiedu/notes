package adapter.interfaceadapter;

public class Client {
    public static void main(String[] args) {
        Vol220 vol220 = new Vol220();
        Phone phone = new Phone();
        Adapter adapter = new Adapter(vol220);
        phone.charging(adapter);
    }
}
