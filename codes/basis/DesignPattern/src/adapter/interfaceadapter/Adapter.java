package adapter.interfaceadapter;

public class Adapter implements Vol5 {
    private Vol220 vol220;

    public Adapter(Vol220 vol220) {
        this.vol220 = vol220;
    }

    @Override
    public int outputVol() {
        int src = vol220.inputVol();
        int dst = src / 44;
        System.out.println("outputVol = " + dst);
        return dst;
    }
}
