package adapter.classadapter;

public class Adapter extends Vol220 implements Vol5 {

    @Override
    public int outputVol() {
        int src = inputVol();
        int dst = src / 44;
        System.out.println("outputVol = " + dst);
        return dst;

    }
}
