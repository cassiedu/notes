package prototype.deepclone;

public class Client {
    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        DeepCopy p = new DeepCopy();
        p.name = "cassie";
        p.deepTarget = new DeepTarget("little cat", "big cat");

        //方式1 完成深拷贝
////		DeepProtoType p2 = (DeepProtoType) p.clone();
////
////		System.out.println("p.name=" + p.name + "p.deepCloneableTarget=" + p.deepCloneableTarget.hashCode());
////		System.out.println("p2.name=" + p.name + "p2.deepCloneableTarget=" + p2.deepCloneableTarget.hashCode());

        //方式2 完成深拷贝
        DeepCopy p2 = (DeepCopy) p.deepClone();
        System.out.println("p.name=" + p.name + "p.deepTarget=" + p.deepTarget.hashCode());
        System.out.println("p2.name=" + p.name + "p2.deepTarget=" + p2.deepTarget.hashCode());

    }
}
