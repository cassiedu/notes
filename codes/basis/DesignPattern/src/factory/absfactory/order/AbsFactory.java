package factory.absfactory.order;

import factory.absfactory.pizza.Pizza;

public interface AbsFactory {
    public abstract Pizza createPizza(String orderType);
}
