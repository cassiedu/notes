package factory.absfactory.order;

import factory.absfactory.pizza.Pizza;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.BufferPoolMXBean;

public class OrderPizza {
    AbsFactory factory;

    public OrderPizza(AbsFactory factory) {
        setFactory(factory);
    }

    private void setFactory(AbsFactory factory) {
        Pizza pizza = null;
        String orderType = ""; //BJ or LD
        this.factory = factory;
        do {
            orderType = getType();
            pizza = factory.createPizza(orderType);
            if (pizza != null) {
                pizza.prepare();
                pizza.bake();
                pizza.box();
                pizza.cut();
            } else {
                System.out.println("order false");
                break;
            }
        } while (true);
    }

    private String getType() {
        try {
            BufferedReader str = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("input pizza class: ");
            String string = str.readLine();
            return string;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
