package factory.absfactory.pizza;

public class BJPepperPizza extends Pizza {

    @Override
    public void prepare() {
        setName("BJPepperPizza");
        System.out.println("BJPepperPizza 需要准备的原材料");
    }
}
