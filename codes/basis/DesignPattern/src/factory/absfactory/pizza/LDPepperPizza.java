package factory.absfactory.pizza;

public class LDPepperPizza extends Pizza {

    @Override
    public void prepare() {
        setName("LDPepperPizza");
        System.out.println("LDPepperPizza 需要准备的原材料");
    }
}
