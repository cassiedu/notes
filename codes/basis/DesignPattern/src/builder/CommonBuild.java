package builder;

public class CommonBuild extends Build {

    @Override
    public void buildBase() {
        System.out.println("commonBuildBase");
    }

    @Override
    public void buildWall() {
        System.out.println("commonBuildWall");
    }

    @Override
    public void buildRoof() {
        System.out.println("commonBuildRoof");
    }
}
