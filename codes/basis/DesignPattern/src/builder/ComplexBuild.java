package builder;

public class ComplexBuild extends Build {

    @Override
    public void buildBase() {
        System.out.println("complexBuildBase");
    }

    @Override
    public void buildWall() {
        System.out.println("complexBuildWall");
    }

    @Override
    public void buildRoof() {
        System.out.println("complexBuildRoof");
    }
}
