package builder;

public class Client {
    public static void main(String[] args) {
        CommonBuild commonBuild = new CommonBuild();
        Director commonDir = new Director(commonBuild);
        House commonHouse = commonDir.construct();
        System.out.println("commonHouse: " + commonHouse);

        ComplexBuild complexBuild = new ComplexBuild();
        Director complexDir = new Director(complexBuild);
        House complexHouse = complexDir.construct();
        System.out.println("complexHouse: " + complexHouse);
    }
}
