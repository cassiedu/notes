package builder;

public abstract class Build {
    protected House house = new House();

    public abstract void buildBase();

    public abstract void buildWall();

    public abstract void buildRoof();

    public House buildHouse() {
        return house;
    }
}
