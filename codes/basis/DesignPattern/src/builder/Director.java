package builder;

public class Director {
    Build build = null;

    public Director(Build build) {
        this.build = build;
    }

//    public void setBuild(Build build) {
//        this.build = build;
//    }

    public House construct() {
        build.buildBase();
        build.buildRoof();
        build.buildWall();
        return build.buildHouse();
    }
}
