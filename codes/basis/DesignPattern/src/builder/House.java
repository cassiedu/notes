package builder;

public class House {
    String base;
    String wall;
    String roof;

    public void setBase(String base) {
        this.base = base;
    }

    public void setWall(String wall) {
        this.wall = wall;
    }

    public void setRoof(String roof) {
        this.roof = roof;
    }

    public String getBase() {
        return base;
    }

    public String getWall() {
        return wall;
    }

    public String getRoof() {
        return roof;
    }
}
