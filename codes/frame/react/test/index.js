class Like extends React.Component {
    Construtor (props) {
        this.state = {
            isLikeMe: false
        }
    }

    handleClick () {
        console.log('handleClick()', this)
        const {isLikeMe} = !this.state
        this.setState({isLikeMe})
    }

    render() {
        const {isLikeMe} = this.state
        return (
            <div>
                <h2 onClick={this.handleClick}>{isLikeMe?'you love me':'me love you'}</h2>
            </div>
        );
    }
}
